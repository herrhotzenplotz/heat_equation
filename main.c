/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Heat equation solver
 *
 *
 * f(x) :: function in 1-d space that rerpresents the temperature at the point
 *         will be an array for us
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define PI M_PI

typedef struct test_config
{
    size_t node_count;
    size_t time_steps;
    double *temperatures;
    double length;
    double alpha;
    double time;
    double t_max;
    double t_min;
    const char *output;
    const char *name;
    void (*generator)(struct test_config*);
} test_config_t;



/* Coefficient calculation
 * Dn = 2/L S (f * sin(n Pi x / L ) dx)
 */
double dn(size_t n, double *temps, double length, size_t elems)
{
    double total = 0,
           dx = length / (double) elems;

    for (size_t i = 0; i < elems; ++i) {
        // coefficient of the integral inside the sum because of
        // possible loss of precision. (depending on the size of L)
        // NOTE: x = i*dx
        total += (2.0 / length) * temps[i] * sin( (double)(i) * dx * PI * (double)(n) ) * dx;
    }

    return total;
}

void heat_solve(double *temperatures,
                double length,
                size_t elems_count,
                double time,
                size_t time_steps,
                double alpha,
                size_t bailout) {

    double dx = length / (double)(elems_count),
           dt = time / (double)(time_steps),
           sum = 0.0,
           t = 0.0,
           x = 0.0,
           d = 0.0,
           sin_factor = 0.0,
           exponential_factor = 0.0;

    // We iterate over the time steps first
    for (size_t t_i = 1; t_i <= time_steps; ++t_i) {
        t = t_i * dt;

        // And now over the x-dimension
        for (size_t x_i = 0; x_i < elems_count; ++x_i) {
            x = dx * x_i;

            // And approximate the infinite sum
            sum = 0.0;
            for (size_t n = 1; n <= bailout; ++n) {
                d = dn(n, temperatures, length, elems_count);
                sin_factor = sin((double)(n) * PI * x / length);
                exponential_factor =
                    exp(-1.0 * (double)(n * n) * PI * PI * alpha * t / ( length * length));
                sum += d * sin_factor * exponential_factor;
            }

            temperatures[t_i*elems_count + x_i] = sum;
        }
    }
}

void gen_temperature_jump(test_config_t *config)
{
    for (size_t i = 0; i < config->node_count; ++i) {
        config->temperatures[i] = i > (config->node_count / 2) ? config->t_max : config->t_min;
    }
}

void gen_temperature_sine(test_config_t *config)
{
    double dx = config->length / (double)(config->node_count);

    for (size_t i = 0; i < config->node_count; ++i) {
        config->temperatures[i] = config->t_max * sin( (double)(i) * dx * PI / config->length );
    }
}

void run_test(test_config_t *config)
{
    FILE *output;
    printf( "INFO : Preparing test '%s'...\n", config->name);

    if (!memset(config->temperatures, 0, sizeof(double) * config->node_count * config->time_steps)) {
        fprintf(stderr, "ERR : Unable to zero out the allocated buffer\n");
        abort();
    }

    config->generator(config);

    output = fopen(config->output, "w");
    if (!output) {
        fprintf(stderr, "ERR : Could not open output file\n");
        abort();
    }

    printf( "INFO : Running test '%s'...\n", config->name);
    heat_solve(config->temperatures,
               config->length,
               config->node_count,
               config->time,
               config->time_steps,
               config->alpha,
               20);

    // Grid of 200x400
    printf( "INFO : Writing out data of test '%s'...\n", config->name);
    for (size_t time = 0; time < config->time_steps; ++time) {
        for (size_t x = 0; x < config->node_count; ++x) {
            fprintf(output, "%lf ", config->temperatures[time*config->node_count + x]);
        }

        fputs("\n", output);
    }

    fclose(output);
}

int main(int argc, char **args)
{
    test_config_t config = (test_config_t) {
        .node_count = 100,
        .time_steps = 200,
        .length = 1.0,
        .alpha = 3.7e-2,
        .time = 4.0,
        .t_max = 373.15,
        .t_min = 298.15,
        .output = NULL,
        .name = NULL,
        .generator = NULL
    };

    config.temperatures = malloc(sizeof(double) * config.node_count * config.time_steps);
    if (!config.temperatures) {
        fprintf(stderr, "ERR : Couldn't malloc temperature array\n");
        abort();
    }

    /* Run with a sinusoidal if called by the makefile
     * otherwise just continue.
     */
    if (argc == 2 && !strcmp(args[1], "make")) {
        config.generator = gen_temperature_sine;
        config.name = "Makefile sample";
        config.output = "output.dat";
        run_test(&config);
        return EXIT_SUCCESS;
    }

    /* Run with a ramp */
    config.generator = gen_temperature_jump;
    config.name = "Ramp";
    config.output = "output.dat";
    run_test(&config);

    /* Run with a sinusoidal*/
    config.generator = gen_temperature_sine;
    config.name = "Sine";
    config.output = "sine.dat";

    run_test(&config);


    return EXIT_SUCCESS;
}
