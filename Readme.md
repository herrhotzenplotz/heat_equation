# Heat equation solution with a Fourier series

![](example.png)

See [Wikipedia](https://en.wikipedia.org/wiki/Heat_equation#Solving_the_heat_equation_using_Fourier_series)

Written in pure C99.

## Requirements

+ C99 compiler
+ gnuplot
+ make
+ PNG image viewer

## Quick start

```bash
$ make plot
$ # feh as an image viewer
$ feh output.png
```

## Tips

You can use the commands from the `plot.gp` file to use the qt backend
of gnuplot to pan around in the 3D-plot. Just don't paste the output
changing commands.

## License

This is licensed under a 3-clause BSD-style license. Please see the
`LICENSE` attached.
