CC=cc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Werror -O0 -ggdb

.PHONY: all
all: plot

plot: output.dat plot.gp
	gnuplot plot.gp

output.dat: heat_solver
	./heat_solver make

heat_solver: main.o
	$(CC) $(CFLAGS) main.o -o $@ -lm

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $^

.PHONY: clean
clean:
	rm heat_solver *.o output.*
