set term pngcairo size 1280,800
set output 'output.png'
set hidden3d offset 0
set title "Heat solution"
set xlabel "position"
set ylabel "time"
set zlabel "temperature"
set pm3d
set view 70,140,,
splot "output.dat" matrix
